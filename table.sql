create table public.transaction_json (
    id serial primary key,
    bank_identifier varchar(255) not null,
    rawjson json not null
);
