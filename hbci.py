#!/usr/bin/env python3

from datetime import date, timedelta, datetime
import json
from settings_secret import *
import logging
import argparse
from adapters.pg import DB


def valid_date(s):
    try:
        return datetime.strptime(s, "%Y-%m-%d")
    except ValueError:
        msg = "Not a valid date: '{0}'.".format(s)
        raise argparse.ArgumentTypeError(msg)

parser = argparse.ArgumentParser(
    description='Retrieve bank account transactions via HBCI/FinTS.')
group = parser.add_mutually_exclusive_group()
group.add_argument('--cron', dest='cron', action='store_const', const=True,
    help='Retrieve transactions and write new ones to the database.')
group.add_argument('--accounts', dest='list_accounts', action='store_const', const=True,
    help='List all accounts for the login.')
group.add_argument('--list', dest='list_transactions', action='store_const', const=True,
    default=True, help='List all transactions. This is the default command.')
parser.add_argument('--begin', '-b', dest='begin', type=valid_date,
    help='The start date as YYYY-MM-DD. Note that some banks do not return transactions older than a few months. Default: 30 days into the past. May be overwritten in settings.py.')
parser.add_argument('--end', '-e', dest='end', type=valid_date,
    # VR Mittelfranken West eG allows e.g. 7 days into the future
    # DKB does not allow it and fails hard
    help='The end date as YYYY-MM-DD. Note that sometimes transactions can be dated in the future. Default: 7 days into the future. May be overwritten in settings.py.')
parser.add_argument('--only', '-o', dest='only',
    help='Limit actions to the named bank account identifier')
args = parser.parse_args()

fints_client_logger = logging.getLogger('fints.client')
@fints_client_logger.addFilter
def fints_useless_warn_filter(record):
    """
    Ignore
    Dialog response: 3060 - Bitte beachten Sie die enthaltenen Warnungen/Hinweise.
    Dialog response: 3076 - Starke Kundenauthentifizierung nicht notwendig.
    Dialog response: 3920 - Zugelassene TAN-Verfahren für den Benutzer (['942', '962', '972', '982'])
    Dialog response: 3010 - *Es liegen keine Umsätze im abgefragten Zeitraum vor
    """
    return not getattr(record, 'fints_response_code', None) in ('3060', '3076', '3920', '3010')


class FTS():
    def __init__(self, dba, args):
        """
        dba is the database adapter which supports process(transaction)
        begin denotes the earliest date for which transactions are fetched
        end denotes the latest date for which transactions are fetched
        """
        self.dba = dba
        self.args = args
        self.banks = list()

    def add_bank(self, short_name, bank_info):
        self.banks.append(Bank(short_name, bank_info, self.args))

    def get_accounts(self):
        for bank in self.banks:
            yield bank.get_accounts()

    def get_transactions(self):
        for bank in self.banks:
            yield bank.list_transactions()

    def list_accounts(self):
        for bank in self.banks:
            bank.list_accounts()

    def list_transactions(self):
        for bank in self.banks:
            bank.list_transactions()

    def cron(self):
        """
        return all new transactions which have been inserted into the database
        """
        for b in self.banks:
            for t in b.get_transactions():
                if self.dba.process(t, b):
                    # attach bank information to new transactions
                    t.data['bank'] = b
                    yield t

class Bank():
    @property
    def c(self):
        from fints.client import FinTS3PinTanClient, FinTSClientMode
        from fints.utils import minimal_interactive_cli_bootstrap
        if not self._client:
            f = FinTS3PinTanClient(
                self.data.get('blz'),
                self.data.get('username'),
                self.data.get('pin'),
                self.data.get('url'),
                customer_id=self.data.get('customer_id', None),
                product_id=PRODUCT_ID,
            )
            f.fetch_tan_mechanisms()
            if self.data.get('tan_methods'):
                f.set_tan_mechanism(str(self.data.get('tan_methods')[0]))
            minimal_interactive_cli_bootstrap(f)
            with f:
                if f.init_tan_response:
                    print("A TAN is required", f.init_tan_response.challenge)
                    tan = input('Please enter TAN:')
                    f.send_tan(f.init_tan_response, tan)
                self.accounts = f.get_sepa_accounts()

                self.transactions = list()
                for acc in self.get_accounts():
                    for t in f.get_transactions(acc, start_date=self.begin, end_date=self.end):
                        self.transactions.append(t)
            self._client = f

        return self._client

    def __init__(self, short_name, bank_info, args):
        self._client = None
        self.accounts = None
        self.transactions = None
        if args.begin:
            self.begin = args.begin
        else:
            self.begin = bank_info.get('default_start_date', date.today() - timedelta(days=30))
        if args.end:
            self.end = args.end
        else:
            self.end = bank_info.get('default_end_date', date.today() + timedelta(days=7))
        self.data = dict(
            short_name=short_name,
            url=bank_info.get('url'),
            username=bank_info.get('username'),
            customer_id=bank_info.get('customer_id'),
            blz=bank_info.get('blz'),
            pin=bank_info.get('pin'),
            tan_methods=bank_info.get('tan_methods'),
            #  default_start_date=bank_info.get('default_start_date'),
            #  default_end_date=bank_info.get('default_end_date'),
        )
        self.c

    def get_accounts(self):
        return self.accounts

    def list_accounts(self):
        for acc in self.get_accounts():
            print(acc, self.c.get_balance(acc)) #, self.c.get_scheduled_debits(acc))

    def get_transactions(self):
        return self.transactions

    def list_transactions(self):
        print(self.get_transactions())

    def __str__(self):
        return self.data.get('short_name')


if __name__ == "__main__":
    db = DB(DB_CONNECTION_STRING)
    f = FTS(db, args)

    if args.only:
        # limit to one bank
        b = ACCOUNTS.get(args.only)
        f.add_bank(args.only, b)
    else:
        for short_name, bank_info in ACCOUNTS.items():
            f.add_bank(short_name, bank_info)

    if args.cron:
        for t in f.cron():
            print(t.data.get('bank'), t.data.get('date'), t.data.get('applicant_name'), t.data.get('amount'), t.data.get('posting_text'), t.data.get('purpose'))
    elif args.list_accounts:
        f.list_accounts()
    elif args.list_transactions:
        f.list_transactions()
