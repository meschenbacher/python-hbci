# HBCI/FinTS

Track your german bank accounts via HBCI/FinTS.

# Requirements

- Postgres database (the postgres adapter also tested with cockroachdb)
- Packages `cron virtualenv`
- A ZKA product id, see https://python-fints.readthedocs.io/en/latest/quickstart.html#register-for-a-product-id

# Install

- set up virtualenv
- create a database and import `table.sql`
- copy `settings_secret.py.example` to `settings_secret.py` and fill in the information
- run `hbci.py --cron` via cron
