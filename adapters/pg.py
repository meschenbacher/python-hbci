import json
import psycopg2
import mt940

class DB():
    """
    postgresql database adapter for connecting and processing transactions
    """
    def __init__(self, connection_string):
        """
        kwargs will be passed to psycopg2.connect
        """
        self._con = None
        self._cur = None
        self.connection_string = connection_string

    @property
    def con(self):
        if not self._con:
            self._con = psycopg2.connect(self.connection_string)
            self._con.autocommit = True
        return self._con

    @property
    def cur(self):
        if not self._cur:
            self._cur = self.con.cursor()
        return self._cur

    def process(self, t, bank):
        """
        check if transaction exists and insert
        return if the transaction is new
        """
        jsonstring = json.dumps(t.data, cls=mt940.JSONEncoder)
        self.cur.execute("""
            SELECT EXISTS(
                SELECT id
                FROM transaction_json
                WHERE %(jsonstring)s::jsonb <@ rawjson::jsonb AND rawjson::jsonb <@ %(jsonstring)s::jsonb
            )""", {'jsonstring': jsonstring})
        exists ,= self.cur.fetchone()
        if not exists:
            self.cur.execute("""
                INSERT INTO transaction_json (
                    rawjson,
                    bank_identifier
                ) VALUES (%s, %s)""", (jsonstring, bank.data.get('short_name'))
            )
            return True
        return False

